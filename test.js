'use strict'

var test = require('tape')
var cloneDeep = require('lodash/cloneDeep')
var redact = require('./')

test('redact.map', function (t) {
  var input = {
    foo: 'non-secret',
    secret: 'secret',
    sub1: {
      foo: 'non-secret',
      password: 'secret'
    },
    sub2: [{
      foo: 'non-secret',
      token: 'secret'
    }]
  }

  var expected = {
    foo: 'non-secret',
    secret: '[REDACTED]',
    sub1: {
      foo: 'non-secret',
      password: '[REDACTED]'
    },
    sub2: [{
      foo: 'non-secret',
      token: '[REDACTED]'
    }]
  }

  var orig = cloneDeep(input)
  var result = redact().map(input)

  t.deepEqual(result, expected)
  t.deepEqual(input, orig)
  t.end()
})
