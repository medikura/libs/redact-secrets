# redact-secrets

Deeply iterate over an object and redact secret values by replacing them
with a predefined string.

[![Build status](https://travis-ci.org/watson/redact-secrets.svg?branch=master)](https://travis-ci.org/watson/redact-secrets)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)

## Installation

```
npm install redact-secrets --save
```

## Usage

```js
//Redact without any otptions
var redact = require('@medikura/redact-secrets')()

// add custom key regex
var redactConstants = require('@medikura/constants')
var customRedact = require('@medikura/redact-secrets')({ keys: [ /email/, ...redactConstants.KEYS ] })

var obj = {
  username: 'watson',
  email: 'test@example.com',
  password: 'hhGu38gf',
  extra: {
    id: 1,
    token: 'some-secret-stuff'
    card: '1234 1234 1234 1234'
  }
}

console.log(redact.map(obj))
// {
//   username: 'watson',
//   email: 'test@example.com',
//   password: '[REDACTED]',
//   extra: {
//     id: 1,
//     token: '[REDACTED]'
//     card: '[REDACTED]'
//   }
// }


console.log(customRedact.map(obj))
// {
//   username: 'watson',
//   email: '[REDACTED]',
//   password: '[REDACTED]',
//   extra: {
//     id: 1,
//     token: '[REDACTED]'
//     card: '[REDACTED]'
//   }
// }
```

## API

### `redact = Redact(options)`

This module exposes a init function which takes an optional object with several options:
* `redactedText` - the `string` used as a replacement variable for values that are redacted, defaults to `"[REDACTED]"`
* `keys` - an array of regexes for keys that are considered secret, defaults to `constants.KEYS`
* `values` - an array of regexes for values that are considered secret, defaults to `constants.VALUES`

The init function returns an object holding one function called `map`

### `redact.map(obj)`

Returns a clone of the given `obj` with its secret values redacted.

## License

MIT

forked from https://github.com/watson/redact-secrets; major changes:
* uses deepdash instead of traverse (traverse is unmaintained)
* allows for adding custom regexes for secret keys and values
