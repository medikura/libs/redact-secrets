'use strict'

const mapValuesDeep = require('deepdash/mapValuesDeep')
const cloneDeep = require('lodash/cloneDeep')
const constants = require('./constants')

module.exports = function ({ redactedText = '[REDACTED]', keys = constants.KEYS, values = constants.VALUES } = {}) {
  return {
    map: map
  }

  function iteratee (value, key) {
    if (keys.some(regex => regex.test(key)) || values.some(regex => regex.test(value))) {
      return redactedText
    }
    return value
  }

  function map (obj) {
    return mapValuesDeep(cloneDeep(obj), iteratee)
  }
}
